function getRandomColor(swatches) {
    var index = Math.round(Math.random() * (swatches.length - 1));
    return swatches[index].color;
}

function fillSelectionRandomly(document) {
    if (!(document.selection instanceof Array)) {
        alert("Please select at least one object and try again.");
        return;
    }

    var swatches = document.swatches.getSelected();
    for (var item of document.selection) {
        if (item.typename == "PathItem") {
            item.filled = true;
            item.fillColor = getRandomColor(swatches);
        }
        if (item.typename == "CompoundPathItem") {
            item.filled = true;
            item.pathItems[0].fillColor = getRandomColor(swatches);
        }
    }
}

fillSelectionRandomly(app.activeDocument);