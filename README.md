# fillSelectionRandomly

fillSelectionRandomly is a script for Adobe Illustrator to fill randomly a selection with swatches colors.

Inspired by [stroke of multiple color vs single color](https://graphicdesign.stackexchange.com/questions/134227/stroke-of-multiple-color-vs-single-color)
and the work of [robotwood](https://github.com/robotwood/Random-Swatch-Fill/blob/master/RandomSwatchesFill.js).